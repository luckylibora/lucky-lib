/**
 * Created by lucky on 02.03.16.
 */

'use strict';

const RedisCache = require('./RedisCache');


class RedisIdCache {


    /**
     *
     * @param {string} name
     * @param {function(Object)} hash
     * @param {Object = null} opts
     */
    constructor(name, hash, opts) {
        this.idCache = new RedisCache(name + ':id', opts);
        this.objCache = new RedisCache(name + ':obj', Object.assign({hash, opts}))
    }


    /**
     *
     * @param {Object} obj
     * @returns {Promise.<Object>}
     */
    add(obj) {
        return Promise.all([
            this.idCache.set(obj.id, obj),
            this.objCache.set(obj, obj.id)
        ]).then(() => obj);
    }


    /**
     *
     * @param {number} id
     * @returns {Promise.<Object>}
     */
    getById(id) {
        return this.idCache.get(id);
    }

    /**
     *
     * @param {Object} obj
     * @returns {Promise.<Object>}
     */
    getByObj(obj) {
        return this.objCache.get(obj)
            .notNull(id => Object.assign({id: id}, obj));
    }

}


module.exports = RedisIdCache;