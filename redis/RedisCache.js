/**
 * Created by lucky on 18.03.16.
 */

'use strict';

const redis = require('./client');


const ttl = 24 * 3600;


class RedisCache {

    /**
     *
     * @param {string} name - name of cache. should be uniq
     * @param {{hash: function = null, ttl: number = null} = null} opts - hash is function which uniquely stringify object
     */
    constructor(name, opts) {
        this.name = name;
        this.ttl = (opts ? opts.ttl : null) || ttl;
        this.hash = (opts ? opts.hash : null) || (arg => arg);
    }


    /**
     *
     * @param {*} obj
     * @returns {Promise}
     */
    get(obj) {
        const hash = this.hash(obj);
        const key = this.redisKey(hash);
        return redis.get(key)
            .notNull(json => JSON.parseWithDate(json));
    }


    /**
     *
     * @param {*} arg
     * @param {function(*)} func
     * @returns {Promise}
     */
    exec(arg, func) {
        return this.get(arg)
            .ifNull(() => {
                return func(arg)
                    .notNull(res => this.set(arg, res));
            });
    }


    /**
     *
     * @param {*} arg
     * @param {function(*)} func
     * @returns {Promise}
     */
    execSync(arg, func) {
        return this.get(arg)
            .ifNull(() => {
                const res = func(arg);
                if (res) {
                    this.set(arg, res);
                }
                return res;
            });
    }


    /**
     *
     * @param {string} hash
     * @returns {string}
     */
    redisKey(hash) {
        return 'cache:' + this.name + ':' + hash;
    }


    /**
     *
     * @param {*} arg
     * @param {*} res
     * @returns {Promise}
     */
    set(arg, res) {
        const hash = this.hash(arg);
        const redisKey = this.redisKey(hash);
        return redis
            .multi(m => {
                return m.set(redisKey, JSON.stringify(res))
                    .expire(redisKey, this.ttl);
            })
            .then(() => res);
    }
}


module.exports = RedisCache;
