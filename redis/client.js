/**
 * Created by lucky on 19.01.16.
 */

const redis = require('redis');
const logger = require('../utils/logger');


const client = redis.createClient(config.redis);

client.on('error', err => logger.error(err));


/**
 *
 */
function dispose() {
    return client.quit();
}



/**
 *
 * @param {string} key
 * @returns {Promise.<string>}
 */
function del(key) {
    if (!key) {
        return Promise.reject(new Error(key));
    }
    return new Promise((s, e) => {
        return client.del(key, err => err ? e(err) : s(key));
    });
}



/**
 *
 * @param {string} key
 * @returns {Promise}
 */
function get(key) {
    return new Promise((s, e) => {
        return client.get(key, (err, res) => err ? e(err) : s(res));
    });
}


/**
 *
 * @param {function(Object)} cmds
 * @returns {Promise}
 */
function multi(cmds) {
    if (!cmds) {
        return Promise.reject(new Error(cmds));
    }
    return new Promise((s, e) => {
        return cmds(client.multi())
            .exec((err, res) => err ? e(err) : s(res));
    });
}


/**
 *
 * @param {string} key
 * @param {string | number} value
 * @returns {Promise.<string>}
 */
function set(key, value) {
    if (!key) {
        return Promise.reject(new Error(key + ' ' + value));
    }
    return new Promise((s, e) => {
        return client.set(key, value, err => err ? e(err) : s(value));
    });
}


exports.client = client;

exports.del = del;

exports.dispose = dispose;

exports.get = get;

exports.multi = multi;

exports.set = set;

