/**
 * Created by lucky on 14.02.16.
 */


const intel = require('intel');


intel.basicConfig({
    format: '[%(date)s] %(levelname)s: %(message)s'
});


module.exports = intel;