/**
 * Created by lucky on 29.02.16.
 */

require('json.date-extensions');


/**
 *
 * @returns {*}
 */
Array.prototype.head = function () {
    return this[0];
};


/**
 *
 * @returns {*}
 */
Array.prototype.last = function () {
    return this[this.length - 1];
};


/**
 *
 * @returns {Array}
 */
Array.prototype.tail = function () {
    if (!this.length) {
        return [];
    }
    return this.slice(1);
};


/**
 *
 * @returns {Date}
 */
Date.prototype.prevDay = function () {
    return new Date(this.getFullYear(), this.getMonth(), this.getDate() - 1);
};


/**
 *
 * @param {Date} d
 * @returns {boolean}
 */
Date.prototype.dateEquals = function (d) {
    return this.getDate() == d.getDate() && this.getMonth() == d.getMonth() && this.getFullYear() == d.getFullYear();
};


/**
 *
 * @returns {Date}
 */
Date.today = function () {
    const now = new Date();
    return new Date(now.getFullYear(), now.getMonth(), now.getDate());
};


/**
 *
 * @returns {Date}
 */
Date.yesterday = function () {
    const now = new Date();
    return new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1);
};

/**
 *
 * @param {function()} onSuccess
 * @returns {Promise}
 */
Promise.prototype.ifNull = function (onSuccess) {
    return this.then(res => res || onSuccess())
};


/**
 *
 * @param {function()} onNull
 * @param {function(*)} onNotNull
 * @returns {Promise}
 */
Promise.prototype.ifNullElse = function (onNull, onNotNull) {
    return this.then(res => !res ? onNull() : onNotNull(res));
};


/**
 *
 * @param {function(*)} onSuccess
 * @returns {Promise}
 */
Promise.prototype.notNull = function (onSuccess) {
    return this.then(res => !res ? null : onSuccess(res));
};



/**
 *
 * @param {number} delay
 * @returns {Promise}
 */
Promise.timeout = function (delay) {
    return new Promise(s => setTimeout(s, delay));
};


/**
 *
 * @param {string} search
 * @returns {string}
 */
String.prototype.removeAll = function (search) {
    return this.replaceAll(search, '');
};


/**
 *
 * @param {string} search
 * @param {string} replacement
 * @returns {string}
 */
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


/**
 *
 * @returns {string}
 */
String.prototype.replaceLineBreaks = function () {
    return this.replace(/(?:\r\n|\r|\n)/g, '');
};


/**
 *
 * @returns {string}
 */
String.prototype.replaceDoubleSpaces = function () {
    return this.replace(/  +/g, ' ');
};


/**
 *
 * @returns {string}
 */
String.prototype.last = function () {
    return this[this.length - 1];
};


/**
 *
 * @returns {string}
 */
String.prototype.removeLast = function () {
    return this.substring(0, this.length - 1);
};
