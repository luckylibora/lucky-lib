/**
 * Created by lucky on 17.02.16.
 */

'use strict';

const Table = require('./Table');


class IdTable extends Table {
    /**
     *
     * @param {{name: string, columns: Array.<string>, schema: String = null}} opts
     */
    constructor(opts) {
        super(opts);
    }

    /**
     *
     * @param {number | string} id
     * @returns {Promise.<Object>}
     */
    getById(id) {
        if (!id) {
            return Promise.reject(new Error(id));
        }
        const query = this.table
            .select(this.table.star())
            .where(this.table.id.equals(id))
            .toQuery();
        return this.getOne(query);
    }

    /**
     *
     * @param {Object} obj
     * @returns {Promise.<Object>}
     */
    getByObj(obj) {
        if (!obj) {
            return Promise.reject(new Error(obj));
        }
        const keys = Object.keys(obj);
        const whereExpressions = [];
        var counter = 0;
        for (var i = 0; i < keys.length; i++) {
            if (obj[keys[i]] == null) {
                whereExpressions.push(`"${keys[i]}" IS NULL`)
            } else {
                counter++;
                whereExpressions.push(`"${keys[i]}"=$${counter}`);
            }
        }
        const where = whereExpressions.join(' AND ');
        const values = keys.map(k => obj[k])
            .filter(v => v != null);
        const sql = ` SELECT * FROM "${this.table._name}" WHERE ${where}`;
        const query = {
            text: sql,
            values: values
        };
        return this.getOne(query);
    }

    /**
     *
     * @param {Object} obj
     * @returns {Promise.<Object>}
     */
    insert(obj) {
        if (!obj) {
            return Promise.reject(new Error(obj));
        }
        const query = this.table
            .insert(obj)
            .toQuery();
        query.text = query.text.replace(';', '') + ' RETURNING id;';
        return super.getOne(query)
            .notNull(row => Object.assign({id: row.id}, obj));
    }


    /**
     *
     * @param {number | string} id
     * @param {Object} obj
     * @returns {Promise}
     */
    updateById(id, obj) {
        if (!id || !obj) {
            return Promise.reject(new Error(id + ' ' + obj));
        }
        const query = this.table
            .update(obj)
            .where(this.table.id.equals(id))
            .toQuery();
        return this.runQuery(query)
            .then(() => obj);
    };


    /**
     *
     * @param {number | string} id
     * @returns {Promise}
     */
    deleteById(id) {
        if (!id) {
            return Promise.reject(new Error(id));
        }
        const query = this.table
            .delete()
            .where(this.table.id.equals(id))
            .toQuery();
        return this.runQuery(query);
    }


    /**
     *
     * @param {number | string} id
     * @param {Object} obj
     * @returns {Promise.<Object>}
     */
    upsertById(id, obj) {
        return this.getById(id)
            .then(row => row ? this.updateById(id, obj) : this.insert(Object.assign({id: id}, obj)));
    }
}


module.exports = IdTable;
