/**
 * Created by lucky on 26.02.16.
 */

'use strict';

const View = require('./View');


class Table extends View {
    /**
     *
     * @param {{name: string, columns: Array.<string>, schema: String = null}} opts
     */
    constructor(opts) {
        super(opts);
    }


    /**
     *
     * @param {Object} data
     * @returns {Promise.<Object>}
     */
    insert(data) {
        if (!data) {
            return Promise.reject(new Error(data));
        }
        const query = this.table
            .insert(data)
            .toQuery();
        return super.runQuery(query)
            .then(() => data);
    }


}


module.exports = Table;

