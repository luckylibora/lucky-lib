/**
 * Created by lucky on 19.01.16.
 */

const pg = require('pg');
const config = require('../config');
const logger = require('../utils/logger');


const Pool = pg.Pool;

const pool = new Pool(config.pg);

pool.on('error', err => logger.error(err));


/**
 *
 * @returns {Promise.<{client, release}>}
 */
function connect() {
    return new Promise((s, e) => pool.connect((err, client, release) => err ? e(err) : s({client, release})));
}


/**
 *
 * @param {number = null} attempt
 * @returns {Promise.<{client: Client, release: function()}>}
 */
function getClient(attempt) {
    //try to reconnect database server 6 times with 10 sec interval
    return connect().catch(err => {
        if (attempt >= 6) {
            return Promise.reject(err);
        }
        logger.error(err, attempt);
        return Promise.timeout(10000)
            .then(() => getClient((attempt || 0) + 1));
    });
}


/**
 *
 * @param {Client} client
 * @param {function()} release
 * @param {string} sql
 * @param {Array} args
 * @returns {Promise.<Array>}
 */
function query(client, release, sql, args) {
    return new Promise((s, e) => {
        return client.query(sql, args, (err, result) => err ? e(err) : s(result.rows));
    }).then(rows => {
        release();
        return rows;
    }).catch(err => {
        logger.error('error running query', sql, args, err);
        return Promise.reject(err);
    });
}


/**
 *
 * @param {string} sql
 * @param {Array = null} args
 * @return {Promise.<Array>}
 */
function run(sql, args) {
    logger.info(sql, args);
    return getClient().then(c => query(c.client, c.release, sql, args));
}


/**
 *
 * @param {{text: string, values: Array}} query
 * @returns {Promise.<Array>}
 */
function runQuery(query) {
    return run(query.text, query.values);
}


/**
 *
 * @param {{text: string, values: Array}} query
 * @returns {Promise.<Object>}
 */
function getOne(query) {
    return runQuery(query).then(rows => rows.length ? rows[0] : null);
}


/**
 *
 */
function dispose() {
    logger.info('Stopping Postgres Client');
    pool.end();
}


exports.dispose = dispose;

exports.getOne = getOne;

exports.runQuery = runQuery;



