/**
 * Created by lucky on 02.03.16.
 */

'use strict';

const IdTable =  require('./IdTable');


class IdNameTable extends IdTable {

    /**
     *
     * @param {Object} opts
     */
    constructor(opts) {
        opts.columns = ['id', 'name'];
        super(opts);
    }

    /**
     *
     * @param {string} name
     * @returns {Promise.<Object>}
     */
    getByName(name) {
        const query = this.table.select(this.table.star())
            .where(this.table.name.equals(name))
            .toQuery();
        return this.getOne(query);
    }

}


module.exports = IdNameTable;
