/**
 * Created by lucky on 20.03.16.
 */

'use strict';

const pg = require('./client');
const sql = require('./sql');


class View {
    /**
     *
     * @param {{name: string, columns: Array.<string>, schema: String = null}} opts
     */
    constructor(opts) {
        this.table = sql.define(opts);
    }

    /**
     *
     * @returns {Promise.<Array>}
     */
    getAll() {
        const query = this.table
            .select(this.table.star())
            .toQuery();
        return this.runQuery(query);
    }

    /**
     *
     * @param {Object} query
     * @returns {Promise.<Object>}
     */
    getOne(query) {
        return pg.getOne(query);
    }

    /**
     *
     * @param {Object} query
     * @returns {Promise.<Array>}
     */
    runQuery(query) {
        return pg.runQuery(query);
    }

}


module.exports = View;


